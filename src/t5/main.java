package t5;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenuItem;

public class main {

	view frame;
	ActionListener list;

	public static void main(String[] args) {
		new main();
	}

	public main() {
		frame = new view();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(300, 400);
		frame.setVisible(true);
		frame.setTitle("My Colors");
		frame.setLocation(510, 160);
		list = new ListenerMgr();
		frame.setListener(list);

	}

	class ListenerMgr extends JMenuItem implements ActionListener {
		Color color = frame.getBackground();

		@Override
		public void actionPerformed(ActionEvent ev) {
			// TODO Auto-generated method stub
			String select = frame.itemRed.getActionCommand();
		

			if (select=="Red") {
				
				color = Color.red;
			}

			if ("Blue".equals(ev.getActionCommand())) {
				color = Color.blue;
			}

			if ("Green".equals(ev.getActionCommand())){
				color = Color.GREEN;
			}
				

				frame.colorPanel.setBackground(color);

		}

	}
}
