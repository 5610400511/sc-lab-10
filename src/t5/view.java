package t5;

import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

public class view extends JFrame{
	JPanel colorPanel,menuPanel;
	JMenuBar mainMenu;
	JMenu menu,menu2;
	JMenuItem itemRed,itemBlue,itemGreen ;
	public view() {
		createPanel();
		createMenuBar();
	}


	public void createPanel() {
		colorPanel = new JPanel();
		menuPanel = new JPanel();
		
		add(menuPanel);
		add(colorPanel);
		

	}
	public  void createMenuBar() {
		//Menu Bar
		mainMenu = new JMenuBar();
		
		//Menu1
		menu = new JMenu("Select");
		itemRed = new JMenuItem("Red");
		itemRed.setMnemonic(KeyEvent.VK_F);
		itemBlue = new JMenuItem("Blue");
		itemBlue.setMnemonic(KeyEvent.VK_F);
		itemGreen = new JMenuItem("Green");
		itemGreen.setMnemonic(KeyEvent.VK_F);

		menu.add(itemRed);
		menu.add(itemBlue);
		menu.add(itemGreen);
		mainMenu.add(menu);
		
		//setmanuBar
		menuPanel.add(mainMenu);
		setJMenuBar(mainMenu);
	
		

	}

	

	public void setListener(ActionListener list) {
		itemRed.addActionListener(list);
		itemBlue.addActionListener(list);
		itemGreen.addActionListener( list);
		

	}

}

