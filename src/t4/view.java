package t4;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

public class view extends JFrame{
	JPanel colorPanel;
	private JPanel buttonPanel;
	JComboBox colorCombo;
	private TitledBorder l1;
	private Border board;
	ButtonGroup group;
	String[] combolist = {"---Color---","Red","Blue","Green"};

	public view() {
		createPanel();
		createPanelButton();

	}

	public void createPanel() {
		colorPanel = new JPanel();
		buttonPanel = new JPanel();
		add(colorPanel, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);

	}

	public void createPanelButton() {
        colorCombo = new JComboBox(combolist);
		buttonPanel.add(colorCombo);	
		board = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
		l1 = BorderFactory.createTitledBorder(board, "Color");
		buttonPanel.setBorder(l1);

	}

	public void setListener(ActionListener list) {
		colorCombo.addActionListener(list);
		

	}

}
