package t1;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

public class View extends JFrame {
	    JPanel colorPanel;
		private JPanel buttonPanel;
		 JButton bRed, bGreen, bBlue;
		private TitledBorder l1;
		private Border board;

		public View() {
			createPanel();
			createPanelButton();
			
		}

		public void createPanel() {
			colorPanel = new JPanel();
			buttonPanel = new JPanel();
			add(colorPanel, BorderLayout.CENTER);
			add(buttonPanel, BorderLayout.SOUTH);
			
		}

		public void createPanelButton() {
			bRed = new JButton("Red");
			bGreen = new JButton("Green");
			bBlue = new JButton("Blue");
			buttonPanel.add(bRed);
			buttonPanel.add(bBlue);
			buttonPanel.add(bGreen);
			board = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
			l1 = BorderFactory.createTitledBorder(board, "Color");
			buttonPanel.setBorder(l1);
			
		}
		public void setListener(ActionListener list) {
			bRed.addActionListener(list);
			bGreen.addActionListener(list);
			bBlue.addActionListener(list);

		}

		
		

		
	}

