package t6;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

public class main {
	

		view frame;
		ActionListener list;
		BankAccount model;

		public void deposite(double amount) {
			model.deposit(amount);
		}

		public void withdraw(double amount) {
			model.withdraw(amount);
		}

		public double getBalance() {
			return model.getBalance();
		}

		public static void main(String[] args) {
			new main();
		}

		public main() {
			frame = new view();
			model = new BankAccount();
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setSize(300, 210);
			frame.setVisible(true);
			frame.setTitle("Transaction");
			frame.setLocation(510, 160);
			list = new ListenerMgr();
			frame.setListener(list);

		}

		class ListenerMgr implements ActionListener {
			int total = 0;

			@Override
			public void actionPerformed(ActionEvent e) {
				String str = frame.text1.getText();
				int input = Integer.parseInt(str);
				if (frame.btDeposit == e.getSource()) {
					model.deposit(input);
					total = (int) model.getBalance();
					frame.show.setText( "Balance :" + " " + total + " ");

				}
				if (frame.btWithdraw == e.getSource()) {
					model.withdraw(input);
					total = (int) model.getBalance();
					frame.show.setText( "Balance :" + " " + total + " ");
				}
			}
		}
	}


