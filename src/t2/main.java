package t2;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;



public class main {
	view frame;
	ActionListener list;

	public static void main(String[] args) {
		new main();
	}

	public main() {
		frame = new view();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(300, 400);
		frame.setVisible(true);
		frame.setTitle("My Colors");
		frame.setLocation(510, 160);
		list = new ListenerMgr();
		frame.setListener(list);

	}

	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == frame.bRed)
				frame.colorPanel.setBackground(Color.RED);
			else if (e.getSource() == frame.bGreen)
				frame.colorPanel.setBackground(Color.GREEN);
			else if (e.getSource() == frame.bBlue)
				frame.colorPanel.setBackground(Color.blue);

			frame.repaint();

		}
	}
}
