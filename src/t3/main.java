package t3;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;



public class main {
view frame;
	ActionListener list;

	public static void main(String[] args) {
		new main();
	}

	public main() {
		frame = new view();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(300, 400);
		frame.setVisible(true);
		frame.setTitle("My Colors");
		frame.setLocation(510, 160);
		list = new ListenerMgr();
		frame.setListener(list);

	}

	class ListenerMgr implements ActionListener {
		Color color = frame.colorPanel.getBackground();
		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == frame.bRed)
				 color =Color.RED;
			if (e.getSource() == frame.bGreen)
				color = Color.GREEN;
		    if (e.getSource() == frame.bBlue)
				color =Color.blue;
		    frame.colorPanel.setBackground(color);
			

		}
	}
}

